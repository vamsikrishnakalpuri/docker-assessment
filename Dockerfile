From alpine:3.13

RUN apk update
RUN apk add nginx
RUN apk add --no-cache openjdk11-jre-headless su-exec curl bash
RUN mkdir /var/www/html

COPY ./default.conf /etc/nginx/conf.d/default.conf
COPY ./SpringBootHello-0.0.1-SNAPSHOT.jar /var/www/html/
COPY ./startup.sh /startup.sh

WORKDIR /var/www/html
VOLUME /var/www/html

EXPOSE 80
RUN chmod +x /startup.sh
CMD [ "/startup.sh" ]
