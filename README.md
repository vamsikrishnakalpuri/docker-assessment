# Assesment Docker 🐳️

## Who am I
- Vamsi Krishna N0639

---

## Build Image
1. docker build -t docker-assesment:N0639 .
2. docker run -it -p 80:80 --name docker-assement-container docker-assesment:N0639

---

## APIs
1. curl localhost/factorial/6
2. curl localhost/fibonacci/10
